angular.module('starter.services', [])

.factory('VeillesFactory', function($http){
  return {
    getAll:function() {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=getAll');
    },
    get:function(veilleId) {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=get&id='+veilleId);
    }
  }
})

.factory('UsersFactory', function($http){
  return {
    getAll:function() {
      return $http.get('http://veille.popschool.fr/api/?api=user&action=getAll');
    },
    get:function(usersId) {
      return $http.get('http://veille.popschool.fr/api/?api=user&action=get&id='+usersId);
    },
    getByUser:function(userId) {
      return $http.get('http://veille.popschool.fr/api/?api=veille&action=getByUser&id_user='+userId);
    },
    auth:function (email, password) {
      return $http.get('http://veille.popschool.fr/api/?api=user&action=auth&email=' + email + '&password=' + password);
    }
  }
})


.factory('Chats', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var chats = [{
    id: 0,
    name: 'Rémy Blancke',
    lastText: 'You know Ntrack Bro?',
    face: 'img/ben.png'
  }, {
    id: 1,
    name: 'Aurélie Cliquet',
    lastText: 'Hey, it\'s me cliqueti',
    face: 'img/max.png'
  }, {
    id: 2,
    name: 'Loïs Marcin',
    lastText: 'I\'m boss in rocket league',
    face: 'img/adam.jpg'
  }, {
    id: 3,
    name: 'Victor Bross',
    lastText: 'I\'m noob!',
    face: 'img/perry.png'
  }, {
    id: 4,
    name: 'Lui',
    lastText: 'Him.',
    face: 'img/mike.png'
  }];

  return {
    all: function() {
      return chats;
    },
    remove: function(chat) {
      chats.splice(chats.indexOf(chat), 1);
    },
    get: function(chatId) {
      for (var i = 0; i < chats.length; i++) {
        if (chats[i].id === parseInt(chatId)) {
          return chats[i];
        }
      }
      return null;
    }
  };
});
