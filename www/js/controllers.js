angular.module('starter.controllers', [])

.controller('VeillesCtrl', function($scope, VeillesFactory){
  $scope.veilles = [];
  VeillesFactory.getAll().then(function(r){
    $scope.veilles = r.data.veilles;
  });
})

.controller('UsersCtrl', function($scope, UsersFactory){
  $scope.users = [];
  UsersFactory.getAll().then(function(r){
    $scope.users = r.data.users;
  });
})

.controller('UserDetailCtrl', function($scope, $stateParams, UsersFactory) {
  UsersFactory.getByUser($stateParams.userId).then(function(r){
    $scope.veilles = r.data.veilles;
    console.log($scope.veilles);
  });
  UsersFactory.get($stateParams.userId).then(function(r){
    $scope.user = r.data.user[0];
    console.log($scope.user);
  });
  console.log($stateParams.userId)
})

.controller('VeilleDetailCtrl', function($scope, $stateParams, VeillesFactory) {
  VeillesFactory.get($stateParams.veilleId).then(function(r){
    $scope.veille = r.data.veille;
    console.log($scope.veille);
  });
  console.log($stateParams.veilleId)
})


.controller('AuthCtrl', function($scope, UsersFactory) {
  $scope.data = {};
  $scope.login = function(){
    console.log($scope.data.password, $scope.data.email);
    UsersFactory.auth($scope.data.email,$scope.data.password).then(function(t){
      if(t.data.status=="ok"){
        // sessionStorage.email = $scope.email;
        console.log("ok");
        return true;
      } else {
        console.log("no");
        return false;
      }
    });
  }
})
.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
})
